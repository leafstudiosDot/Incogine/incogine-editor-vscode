// The module 'vscode' contains the VS Code extensibility API
// Import the module and reference it with the alias vscode in your code below
import * as vscode from 'vscode';
import * as fs from 'fs';
import * as path from 'path';
import * as xml2js from 'xml2js';

// This method is called when your extension is activated
// Your extension is activated the very first time the command is executed
export function activate(context: vscode.ExtensionContext) {

	// Use the console to output diagnostic information (console.log) and errors (console.error)
	// This line of code will only be executed once when your extension is activated
	console.log('Incogine Editor is now active!');

	// The command has been defined in the package.json file
	// Now provide the implementation of the command with registerCommand
	// The commandId parameter must match the command field in package.json
	//let disposable = vscode.commands.registerCommand('incogineeditor.helloWorld', () => {
	// The code you place here will be executed every time your command is executed
	// Display a message box to the user
	//vscode.window.showInformationMessage('Hello World from IncogineEditor!');
	//});

	//context.subscriptions.push(disposable);

	const projectView = vscode.window.createTreeView('incogineeditor', {
		treeDataProvider: new ProjectTreeDataProvider()
	});

	context.subscriptions.push(projectView);

	context.subscriptions.push(vscode.commands.registerCommand('extension.openProjectSettings', () => {
		const workspaceFolders = vscode.workspace.workspaceFolders;
		if (workspaceFolders) {
			const projectPath = path.join(workspaceFolders[0].uri.fsPath, 'src', 'project.xml');
			if (fs.existsSync(projectPath)) {
				const panel = vscode.window.createWebviewPanel(
					'projectSettings',
					'Project Settings',
					vscode.ViewColumn.One,
					{
						enableScripts: true
					}
				);

				// Read and parse the XML file
				fs.readFile(projectPath, 'utf8', (err, data) => {
					if (err) {
						vscode.window.showErrorMessage(`Failed to read project.xml: ${err.message}`);
						return;
					}

					xml2js.parseString(data, (err, result) => {
						if (err) {
							vscode.window.showErrorMessage(`Failed to parse project.xml: ${err.message}`);
							return;
						}

						const project = result.project;
						panel.webview.html = getWebviewContent(project);

						// Handle messages from the webview
						panel.webview.onDidReceiveMessage(
							message => {
								switch (message.command) {
									case 'save':
										const builder = new xml2js.Builder();
										const xml = builder.buildObject({ project: message.project });

										fs.writeFile(projectPath, xml, (err) => {
											if (err) {
												vscode.window.showErrorMessage(`Failed to save project.xml: ${err.message}`);
											} else {
												vscode.window.showInformationMessage('Project settings saved successfully.');
											}
										});
										return;
								}
							},
							undefined,
							context.subscriptions
						);
					});
				});
			} else {
				vscode.window.showInformationMessage('project.xml not found in the current workspace.');
			}
		} else {
			vscode.window.showInformationMessage('No folder or workspace opened.');
		}
	}));

}


class ProjectTreeDataProvider implements vscode.TreeDataProvider<vscode.TreeItem> {
	private _onDidChangeTreeData: vscode.EventEmitter<vscode.TreeItem | undefined | null | void> = new vscode.EventEmitter<vscode.TreeItem | undefined | null | void>();
	readonly onDidChangeTreeData: vscode.Event<vscode.TreeItem | undefined | null | void> = this._onDidChangeTreeData.event;

	getTreeItem(element: vscode.TreeItem): vscode.TreeItem {
		return element;
	}

	getChildren(element?: vscode.TreeItem): Thenable<vscode.TreeItem[]> {
		return new Promise((resolve, reject) => {
			const workspaceFolders = vscode.workspace.workspaceFolders;
			if (workspaceFolders) {
				const projectPath = path.join(workspaceFolders[0].uri.fsPath, 'src', 'project.xml');
				if (fs.existsSync(projectPath)) {
					//resolve([]);

					if (!element) {
						// Root elements (collapsible menus)
						resolve([
							new TreeItem('Build', vscode.TreeItemCollapsibleState.Collapsed),
							new TreeItem('Settings', vscode.TreeItemCollapsibleState.Collapsed)
						]);
					} else {
						// Child elements (items within menus)
						switch (element.label) {
							case 'Build':
								resolve([]);
								break;
							case 'Settings':
								resolve([
									new TreeItem('Project Settings', vscode.TreeItemCollapsibleState.None, {
										command: 'extension.openProjectSettings',
										title: '',
										arguments: []
									})
								]);
								break;
							default:
								resolve([]);
						}
					}
				} else {
					resolve([new vscode.TreeItem('This directory/workspace is not an Incogine project.')]);
				}
			} else {
				resolve([new vscode.TreeItem('No directory/workspace opened.')]);
			}
		});
	}
}

class TreeItem extends vscode.TreeItem {
	constructor(
		public readonly label: string,
		public readonly collapsibleState: vscode.TreeItemCollapsibleState,
		public readonly command?: vscode.Command
	) {
		super(label, collapsibleState);
		this.tooltip = `${this.label}`;
		this.description = this.label;
	}
}

// This method is called when your extension is deactivated
export function deactivate() { }

function getWebviewContent(project: any): string {
	return `
	  <!DOCTYPE html>
	  <html lang="en">
	  <head>
		<meta charset="UTF-8">
		<meta name="viewport" content="width=device-width, initial-scale=1.0">
		<title>Project Settings</title>
	  </head>
	  <body>
		<h1>Project Settings</h1>
		<form id="project-form">
		  <label for="name">Name:</label><br>
		  <input type="text" id="name" name="name" value="${project.name}"><br>
		  <label for="version">Version:</label><br>
		  <input type="text" id="version" name="version" value="${project.version}"><br>
		  <label for="description">Description:</label><br>
		  <input type="text" id="description" name="description" value="${project.description}"><br>
		  <label for="incogine_version">Incogine Version:</label><br>
		  <input type="text" id="incogine_version" name="incogine_version" value="${project.incogine_version}"><br>
		  <label for="author">Author:</label><br>
		  <input type="text" id="author" name="author" value="${project.author}"><br><br>
		  <button type="button" onclick="save()">Save</button>
		</form>
  
		<script>
		  const vscode = acquireVsCodeApi();
  
		  function save() {
			const project = {
			  name: document.getElementById('name').value,
			  version: document.getElementById('version').value,
			  description: document.getElementById('description').value,
			  incogine_version: document.getElementById('incogine_version').value,
			  author: document.getElementById('author').value
			};
  
			vscode.postMessage({
			  command: 'save',
			  project: project
			});
		  }
		</script>
	  </body>
	  </html>
	`;
  }